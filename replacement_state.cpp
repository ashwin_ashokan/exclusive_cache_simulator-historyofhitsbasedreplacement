#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <map>
#include <iostream>

using namespace std;

#include "replacement_state.h"
#define MAX_COUNTER_VALUE 7 //saturating value of RRPV, i.e has the highest re-reference
#define LONG_REFERENCE (MAX_COUNTER_VALUE-3) //represents intermediate RRPV
long int countHits =0;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This file is distributed as part of the Cache Replacement Championship     //
// workshop held in conjunction with ISCA'2010.                               //
//                                                                            //
//                                                                            //
// Everyone is granted permission to copy, modify, and/or re-distribute       //
// this software.                                                             //
//                                                                            //
// Please contact Aamer Jaleel <ajaleel@gmail.com> should you have any        //
// questions                                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

/*
** This file implements the cache replacement state. Users can enhance the code
** below to develop their cache replacement ideas.
**
*/


////////////////////////////////////////////////////////////////////////////////
// The replacement state constructor:                                         //
// Inputs: number of sets, associativity, and replacement policy to use       //
// Outputs: None                                                              //
//                                                                            //
// DO NOT CHANGE THE CONSTRUCTOR PROTOTYPE                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
CACHE_REPLACEMENT_STATE::CACHE_REPLACEMENT_STATE( UINT32 _sets, UINT32 _assoc, UINT32 _pol )//_pol is policy type
{

    numsets    = _sets;
    assoc      = _assoc;
    replPolicy = _pol;

    mytimer    = 0;

    InitReplacementState();
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// The function prints the statistics for the cache                           //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
ostream & CACHE_REPLACEMENT_STATE::PrintStats(ostream &out)
{

    out<<"=========================================================="<<endl;
    out<<"=========== Replacement Policy Statistics ================"<<endl;
    out<<"=========================================================="<<endl;

    // CONTESTANTS:  Insert your statistics printing here
    out<<"THe Number of Hits in LLC Cache with L1 - SHIP: "<<countHits<<endl;
    return out;

}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This function initializes the replacement policy hardware by creating      //
// storage for the replacement state on a per-line/per-cache basis.           //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

void CACHE_REPLACEMENT_STATE::InitReplacementState()
{
    // Create the state for sets, then create the state for the ways

    repl  = new LINE_REPLACEMENT_STATE* [ numsets ]; //repl holds the pointer to array of pointer to all the blocks. where repl[set][block].parameter

    // ensure that we were able to create replacement state

    assert(repl);

    // Create the state for the sets
    for(UINT32 setIndex=0; setIndex<numsets; setIndex++) 
    {
        repl[ setIndex ]  = new LINE_REPLACEMENT_STATE[ assoc ];

        for(UINT32 way=0; way<assoc; way++) 
        {
            // initialize stack position (for true LRU)
            repl[ setIndex ][ way ].LRUstackposition = way; //It is a pointer or pointers.
        }
    }

    if (replPolicy != CRC_REPL_CONTESTANT) return;

    // Contestants:  ADD INITIALIZATION FOR YOUR HARDWARE HERE
    for(UINT32 setIndex = 0; setIndex<numsets; setIndex++) //initializing the replacement state for all cache Levels.
    {
        for(UINT32 way = 0;way<assoc;way++)
        {
            repl[setIndex][way].counter = MAX_COUNTER_VALUE;
            repl[setIndex][way].outcome = false;
            repl[setIndex][way].signature_m = 0;
            repl[setIndex][way].is_prefetched = false;

        }
    }
        for(UINT32 index=0;index<sizeof(shct);index++)
            {
                shct[index] = 4;    //inital value of signature based counter   
            }
        
}


////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This function is called by the cache on every cache miss. The input        //
// argument is the set index. The return value is the physical way            //
// index for the line being replaced.                                         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
INT32 CACHE_REPLACEMENT_STATE::GetVictimInSet( UINT32 tid, UINT32 setIndex, const LINE_STATE *vicSet, UINT32 assoc, Addr_t PC, Addr_t paddr, UINT32 accessType, UINT32 accessSource ) {
    // If no invalid lines, then replace based on replacement policy
    if( replPolicy == CRC_REPL_LRU ) 
    {
        return Get_LRU_Victim( setIndex );
    }
    else if( replPolicy == CRC_REPL_RANDOM )
    {
        return Get_Random_Victim( setIndex );
    }
    else if( replPolicy == CRC_REPL_CONTESTANT )
    {
        // Contestants:  ADD YOUR VICTIM SELECTION FUNCTION HERE
	return Get_My_Victim (tid,setIndex,vicSet,assoc,PC,paddr,accessType,accessSource);
    }

    // We should never reach here because there is no else hence sometype of policy must be chosen

    assert(0);
    return -1;
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This function is called by the cache after every cache hit/miss            //
// The arguments are: the set index, the physical way of the cache,           //
// the pointer to the physical line (should contestants need access           //
// to information of the line filled or hit upon), the thread id              //
// of the request, the PC of the request, the accesstype, and finall          //
// whether the line was a cachehit or not (cacheHit=true implies hit),        //  
// access source->whether from L1->L2 writeback or anything similar.          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
void CACHE_REPLACEMENT_STATE::UpdateReplacementState( 
    UINT32 setIndex, INT32 updateWayID, const LINE_STATE *currLine,UINT32 tid, Addr_t PC, UINT32 accessType, bool cacheHit, UINT32 accessSource)
{
    // What replacement policy?
    if( replPolicy == CRC_REPL_LRU ) 
    {
        UpdateLRU( setIndex, updateWayID );
    }
    else if( replPolicy == CRC_REPL_RANDOM )
    {
        // Random replacement requires no replacement state update
    }
    else if( replPolicy == CRC_REPL_CONTESTANT )
    {
        // Contestants:  ADD YOUR UPDATE REPLACEMENT STATE FUNCTION HERE
        // Feel free to use any of the input parameters to make
        // updates to your replacement policy
        UpdateMyPolicy(setIndex,updateWayID,currLine,tid,PC,accessType,cacheHit,accessSource);
    }
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//////// HELPER FUNCTIONS FOR REPLACEMENT UPDATE AND VICTIM SELECTION //////////
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This function finds the LRU victim in the cache set by returning the       //
// cache block at the bottom of the LRU stack. Top of LRU stack is '0'        //
// while bottom of LRU stack is 'assoc-1'                                     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
INT32 CACHE_REPLACEMENT_STATE::Get_LRU_Victim( UINT32 setIndex )
{
	// Get pointer to replacement state of current set

	LINE_REPLACEMENT_STATE *replSet = repl[ setIndex ];
	INT32   lruWay   = 0;

	// Search for victim whose stack position is assoc-1

	for(UINT32 way=0; way<assoc; way++) {
		if (replSet[way].LRUstackposition == (assoc-1)) {
			lruWay = way;
			break;
		}
	}

	// return lru way

	return lruWay;
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This function finds a random victim in the cache set                       //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
INT32 CACHE_REPLACEMENT_STATE::Get_Random_Victim( UINT32 setIndex )
{
    INT32 way = (rand() % assoc);
    
    return way;
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This function implements the LRU update routine for the traditional        //
// LRU replacement policy. The arguments to the function are the physical     //
// way and set index.                                                         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

void CACHE_REPLACEMENT_STATE::UpdateLRU( UINT32 setIndex, INT32 updateWayID )
{
	// Determine current LRU stack position
	UINT32 currLRUstackposition = repl[ setIndex ][ updateWayID ].LRUstackposition;

	// Update the stack position of all lines before the current line
	// Update implies incremeting their stack positions by one

	for(UINT32 way=0; way<assoc; way++) {
		if( repl[setIndex][way].LRUstackposition < currLRUstackposition ) {
			repl[setIndex][way].LRUstackposition++;// ## Very Inefficient Way for Doing it.
		}
	}

	// Set the LRU stack position of new line to be zero
	repl[ setIndex ][ updateWayID ].LRUstackposition = 0;//repl is a pointer of pointer type.Hence we choose the repl[set][block_num]
}

INT32 CACHE_REPLACEMENT_STATE::Get_My_Victim(UINT32 tid , UINT32 setIndex, const LINE_STATE *vicSet, UINT32 assoc, Addr_t PC, Addr_t paddr, UINT32 accessType, UINT32 accessSource) 
{ //Function for choosing your victim.
	// return first way always
    
    if(assoc==0)
    { 
        //if(assoc==16)cout<<"Assoc: "<<assoc<<endl;
        cout<<"Shouldnt get here,debug statement"
        return Get_LRU_Victim(setIndex);//For L2 and L3 Cache use LRU and L1 use SHiP Policy.
    }

    else
    {
    LINE_REPLACEMENT_STATE * set = repl[setIndex];
    bool done = false;
    UINT32 chosen_line = 0;

	while(!done)
    {
        for(UINT32 way=0;way<assoc;way++)
        {
            if(set[way].counter == MAX_COUNTER_VALUE && done == false)  //Finds the first line with the max counter value
            {
                done = true;
                chosen_line = way;
            }       
        }

        if(!done)       //increment all rrpv values
        {
            for(UINT32 way=0;way<assoc;way++)
            {
                if(set[way].counter < MAX_COUNTER_VALUE)
                {
                    set[way].counter++;
                }
            }
    
        }
    }   
    if(done)
    {
        return chosen_line;
    }
    else
    {
       cerr<<"Shouldnt Come Here,Debug-GetMyVictim"
    }
}
}




void CACHE_REPLACEMENT_STATE::UpdateMyPolicy(UINT32 setIndex, INT32 updateWayID, const LINE_STATE *currLine,UINT32 tid, Addr_t PC, UINT32 accessType, bool cacheHit, UINT32 accessSource) 
{ //function for updating the replacement state,whenever there is a hit, you cant do anything because it gets invalidated in LLC
    //The update My policy can be changed by using the method for gener
    if(assoc==0) {
        cerr<<"Shouldnt Come Here"<<endl;
        UpdateLRU(setIndex,updateWayID);

    }

    else{
    if(!cacheHit) countHits++;
    bool is_prefetch = false;
    if(accessType==ACCESS_PREFETCH)
    {
        is_prefetch = true;

    }

    LINE_REPLACEMENT_STATE *set = repl[setIndex];
    UINT32 signature = (UINT32)( ((PC>>18)) % (sizeof(shct)));
    //cout<<(PC+is_prefetch)%sizeof(shct)<<endl;
    if(accessType == ACCESS_WRITEBACK)          //optimization #2
    {
        set[updateWayID].counter = 4;
        return;
    }

    if(cacheHit)
    {
        if(accessType == ACCESS_PREFETCH && set[updateWayID].is_prefetched ==false)
        {
            set[updateWayID].is_prefetched = true;
            set[updateWayID].counter = 2;
            return;
        }
        set[updateWayID].is_prefetched=false;
        set[updateWayID].counter =  0;
        set[updateWayID].outcome = true;
        if(shct[set[updateWayID].signature_m] < 3) //two bit value, hence we update the SHCT counter to the max value of 3(0-3)
        {
            shct[set[updateWayID].signature_m]++;
        
        }

    }

    else //not a hit, it is a miss.
    {
        if(set[updateWayID].outcome != true)
        {
            if(shct[set[updateWayID].signature_m]>0) 
            {
                shct[set[updateWayID].signature_m]--;
            }
        }

        set[updateWayID].outcome = false;
        set[updateWayID].signature_m=signature;
        if(shct[signature]==0)
        {
            set[updateWayID].counter = 6;
        }
        else
        {
            set[updateWayID].counter=LONG_REFERENCE;
        }
    }

}
}

CACHE_REPLACEMENT_STATE::~CACHE_REPLACEMENT_STATE (void) {
}
