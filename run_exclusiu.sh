#!/bin/bash

# Requires having a folder "traces" in the same directory with all the traces
# You can change it to a different path if the traces are somewhere else

TRACE_DIR=./
SUMMARY=summary.out
LRU_FILE=tmp_lru.out
STD_FILE=tmp_std.out

make

rm -f ${SUMMARY}

prod=1
count=0
for f in $/.gz
do
    name="$(basename "${f%.*}"|cut -d'.' -f1,2) "
    name="$(echo -e "${name}" | sed -e 's/[[:space:]]*$//')"

    export DAN_POLICY=0 && ./exclusiu $f > ${LRU_FILE} 2> /dev/null 
    IPC_LRU=`grep "IPC" tmp_lru.out |cut -d' ' -f3`

    export DAN_POLICY=2 && ./exclusiu $f > ${STD_FILE} 2> /dev/null 
    IPC_student=`grep "IPC" tmp_std.out |cut -d' ' -f3`

    speedup=$( bc -l <<< "scale=10; ${IPC_student}/${IPC_LRU}" )
    ((count++))

    echo "${name},${speedup}" >> ${SUMMARY}
    prod=$( bc -l <<< "scale=10;${prod}*${speedup}" )
done

geomean=$( bc -l <<< "scale=10; ( (e(l(${prod})/${count})) - 1 )*100" )

echo "geomean=${geomean}%"
printf "\n\ngeomean=${geomean}" >> ${SUMMARY}

rm ${LRU_FILE} ${STD_FILE}
