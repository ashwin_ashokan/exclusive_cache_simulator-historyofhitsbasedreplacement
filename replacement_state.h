#ifndef REPL_STATE_H
#define REPL_STATE_H
 
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// This file is distributed as part of the Cache Replacement Championship     //
// workshop held in conjunction with ISCA'2010.                               //
//                                                                            //
//                                                                            //
// Everyone is granted permission to copy, modify, and/or re-distribute       //
// this software.                                                             //
//                                                                            //
// Please contact Aamer Jaleel <ajaleel@gmail.com> should you have any        //
// questions                                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <cassert>
#include "utils.h"
#include "crc_cache_defs.h"
#include <iostream>

using namespace std;

// Replacement Policies Supported
//We could always use RRIP in L1 Cache since it is also an inclusive Cache and doesnt have to be Invalidate.Or 
//L1 Can Also Use SHiP - since, there are always hits that are not invalidated. And can Be used to retain the most useful blocks in Memory
//For L2 This Filtered Recency Can Be Used with RRIP (Based on if it has been used in )
//::Declare a Big Array Whose size equals the entire cache to hold the LinesState Between The Caches,On Getting a Hit,or on Invalidation.
/*Possibilities for Reducing The Miss Rate:
 * Using Belady's Replacement Policy In Case of Eviction,
 * Using SFL Bit- Understanding its working -  Making Use of L2 Replacement Policy 

*/
typedef enum 
{
    CRC_REPL_LRU        = 0,
    CRC_REPL_RANDOM     = 1,
    CRC_REPL_CONTESTANT = 2
} ReplacemntPolicy;

// Replacement State Per Cache Line
typedef struct
{
    UINT32  LRUstackposition;
    // CONTESTANTS: Add extra state per cache line here
    UINT32 counter;
    UINT32 signature_m;
    bool outcome;
    bool is_prefetched;

    

} LINE_REPLACEMENT_STATE;// Each cache line has its own replacement state such as LRU_Position or counter_bit,per_block metadata

struct sampler; // Jimenez's structures

// The implementation for the cache replacement policy
class CACHE_REPLACEMENT_STATE //Other fields and data for the entire cache such as cache table.
//Present for each Cache level L1,L2,L3
{
public:
    LINE_REPLACEMENT_STATE   **repl; //For numsets and assoc we have pointer of pointers
  private:

    UINT32 numsets;
    UINT32 assoc;
    UINT32 replPolicy;
    

    COUNTER mytimer;  // tracks # of references to the cache or accesses for each cache level
    // CONTESTANTS:  Add extra state for cache here
    unsigned char shct[14000];//SHCT of 14000 entries for every cache initialization.
  public:
    ostream & PrintStats(ostream &out);

    // The constructor CAN NOT be changed
    CACHE_REPLACEMENT_STATE( UINT32 _sets, UINT32 _assoc, UINT32 _pol );

    INT32 GetVictimInSet( UINT32 tid , UINT32 setIndex, const LINE_STATE *vicSet, UINT32 assoc, Addr_t PC, Addr_t paddr, UINT32 accessType, UINT32 accessSource); //LIN_STATE - Structure LIN_STATE that has a tag of 64bit address, just a declaration
    //it is just a declaration so it can be any parameter.

    void   UpdateReplacementState( UINT32 setIndex, INT32 updateWayID);// 

    void   SetReplacementPolicy( UINT32 _pol ) { replPolicy = _pol; } //chooses replacement policy
    void   IncrementTimer() { mytimer++; } //increments the timer for each access

    void   UpdateReplacementState( UINT32 setIndex, INT32 updateWayID, const LINE_STATE *currLine, //holds the tag value  currLine.tag give the tag value
                                   UINT32 tid, Addr_t PC, UINT32 accessType, bool cacheHit, UINT32 accessSource);
                                   //This function is called whenever there is a cache line access i.e when a hit or miss in each cache level

    ~CACHE_REPLACEMENT_STATE(void);//destructor

  private:
    
    void   InitReplacementState();//Initialize the replacement state for each line percache/perline basis
    INT32  Get_Random_Victim( UINT32 setIndex );

    INT32  Get_LRU_Victim( UINT32 setIndex );
    INT32  Get_My_Victim( UINT32 tid , UINT32 setIndex, const LINE_STATE *vicSet, UINT32 assoc, Addr_t PC, Addr_t paddr, UINT32 accessType, UINT32 accessSource);// Chooses the victim based on replacement policy
    void   UpdateLRU( UINT32 setIndex, INT32 updateWayID );
    void   UpdateMyPolicy(UINT32 setIndex, INT32 updateWayID, const LINE_STATE *currLine,UINT32 tid, Addr_t PC, UINT32 accessType, bool cacheHit, UINT32 accessSource);//This is called by updateReplacementState func
};
#endif
